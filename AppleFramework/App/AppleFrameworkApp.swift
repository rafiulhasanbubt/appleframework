//
//  AppleFrameworkApp.swift
//  AppleFramework
//
//  Created by rafiul hasan on 12/10/21.
//

import SwiftUI

@main
struct AppleFrameworkApp: App {
    var body: some Scene {
        WindowGroup {
            FrameworkGridView()
        }
    }
}
