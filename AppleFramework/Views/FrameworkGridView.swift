//
//  FrameworkGridView.swift
//  AppleFramework
//
//  Created by rafiul hasan on 12/10/21.
//

import SwiftUI

struct FrameworkGridView: View {
    @StateObject private var frameworkVM = FrameworkGridViewModel()
    
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVGrid(columns: frameworkVM.columns) {
                    ForEach(MockData.frameworks) { framework in
                        FrameworkTitleView(framework: framework)
                            .onTapGesture {
                                frameworkVM.selectedFramework = framework
                            }
                    }
                }
                .navigationTitle("🍎 Frameworks")
                .fullScreenCover(isPresented: $frameworkVM.isShowingDetailView) {
                    FrameworkDetailView(viewModel: FrameworkDetailViewModel(framework: frameworkVM.selectedFramework!, isShowingDetailView: $frameworkVM.isShowingDetailView))
                }
            }
        }
    }
}

struct FrameworkGridView_Previews: PreviewProvider {
    static var previews: some View {
        FrameworkGridView()
            .preferredColorScheme(.dark)
    }
}
