//
//  XDismissButtonView.swift
//  AppleFramework
//
//  Created by rafiul hasan on 13/10/21.
//

import SwiftUI

struct XDismissButtonView: View {
    @Binding var isShowingDetailView: Bool
    
    var body: some View {
        HStack {
            Spacer()
            
            Button {
                isShowingDetailView = false
            } label: {
                Image(systemName: "xmark")
                    .foregroundColor(Color(.label))
                    .imageScale(.large)
                    .frame(width: 44, height: 44)
            }
        }
        .padding()
    }
}

struct XDismissButtonView_Previews: PreviewProvider {
    static var previews: some View {
        XDismissButtonView(isShowingDetailView: .constant(false))
    }
}
