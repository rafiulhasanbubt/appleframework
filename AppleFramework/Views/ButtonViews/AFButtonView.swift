//
//  AFButtonView.swift
//  AppleFramework
//
//  Created by rafiul hasan on 12/10/21.
//

import SwiftUI

struct AFButtonView: View {
    var title: String
    
    var body: some View {
        Text(title)
            .font(.title2)
            .fontWeight(.semibold)
            .frame(width: 300, height: 50)
            .background(Color.red)
            .foregroundColor(.white)
            .cornerRadius(10)
    }
}

struct AFButtonView_Previews: PreviewProvider {
    static var previews: some View {
        AFButtonView(title: "Test Title")
    }
}
